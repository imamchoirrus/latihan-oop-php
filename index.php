<?php
require_once('sheep.php');
require_once('frog.php');
require_once('ape.php');
$sheep = new Animal("Shaun");

echo "Name : ". $sheep->name. "<br>"; // "shaun"
echo "Legs : ". $sheep->legs. "<br>"; // 4
echo "Cold Blooded : ". $sheep->cold_blooded. "<br><br>"; // "no"

$kodok = new Frog("Buduk");
echo "Name : ". $kodok->name. "<br>"; // "buduk"
echo "Legs : ". $kodok->legs. "<br>"; // 4
echo "Cold Blooded : ". $kodok->cold_blooded. "<br>";
echo $kodok->jump(). "<br><br>";

$sungokong = new Ape("Kera Sakti");
echo "Name : ". $sungokong->name. "<br>"; // "kera sakti"
echo "Legs : ". $sungokong->legs. "<br>"; // 42
echo "Cold Blooded : ". $sungokong->cold_blooded. "<br>";
echo $sungokong->yell();

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
?>